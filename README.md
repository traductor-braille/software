# Traductor Braile

<p> It is a final project for Informatica II: Electronic Engineering UTN FRC </p>
<p> The Traductor Braille project consists of a portable device that converts, letter by letter, a stored or sent text into characters of the Braille alphabet. </p>

## Descripton

With the present work, we will seek to provide an alternative to improve the quality of education of blind people.

## Hardware Requirements

* Arduino with Atmega328p processor
* Micro SD Card Reader Module
* 3 Micro Servo 9g
* 3 cylinders with indicated combinations (read Hardware)

## Technical Requirements
In process...

### Program Operation
In process...

### Device Operation
In process...

## Block Diagram
In process...


## Software Instalation
### Development 
To download this repository, in a terminal type

```
mkdir traductorBraille
cd traductorBraille
git clone https://gitlab.com/traductor-braille/software.git
cd software
```

### Branchs description
In process...

## Contributors

* Matias Mollecker (Developer)
* Simon Soardo (Developer)
* Matias Leiva (Developer)

## Sponsor
Desire to pass the subject.
