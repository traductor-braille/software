#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include "khbit_loop.h"

static struct termios initial_settings, new_settings;

/* creamos variable estatica de control */
static int peek_character = -1;

void khbit_loop(int *fd, FILE *fd_log){
	/* caracteres de lectura escritura */
	char letrain=0, letraout=0;
	/* caracteres leidos en el puerto */
	int cuenta;

	init_keyboard();

	while( (letrain != 4) && (letraout != 4) ){  // <CTRL-D>
		if(kbhit()){
			letraout = readch();
			if(fd_log != NULL)
				/* 'fwrite' escribe 'sizeof(letraout)' elementos de datos
				 * de tamanio 'sizeof(char)' en el puntero a FILE 'fd_log'
				 * y obtiene los datos con el apuntador a 'letraout'*/
				fwrite(&letraout, sizeof(char), sizeof(letraout), fd_log);

			/* Si llegamos a un caracter de fin de linea
			 * hacemos un retorno de carro */
			if(letraout == 10)
				letraout = 13;

			/* 'write' nos permite comunicarnos con el puerto serial, copiando
			 * líneas de la terminal al archivo del puerto 'fd' */
			write( *fd, &letraout, 1 ); /* Escribe en el puerto*/


			/* Para flujos de salida, fflush() fuerza una escritura de todos
			 * los datos almacenados en el búfer del espacio de usuario para
			 * el flujo de salida o actualización dado a través de la función
			 * de escritura subyacente del flujo */
			fflush( NULL );

			/* 'tcflush()' junto con 'TCIOFLUSH' vacia tanto los datos recibidos
			* pero no leidos, como los datos escritos pero no transmitidos */
			//tcflush(*fd,TCIOFLUSH);

			/* 'tcdrain()' espera hasta que se haya transmitido toda la salida
			 * escrita en el puerto */
			//tcdrain(*fd);
		}

		/* 'read()' intenta leer '1' bytes del descriptor de archivo fd
		 * en el búfer a partir del apuntador a 'letrain' */
		cuenta = read(*fd, &letrain, 1);

		if( cuenta > 0 ){
			if (fd_log != NULL)
				fwrite(&letrain, sizeof(char), sizeof(letrain), fd_log);

			write( 1, &letrain, 1 );
			fflush( NULL );
		}

		/* Se espera TD_LOOP segundos entre lecturas
		 * para reducir el uso de la CPU */
		sleep(TD_LOOP);
    }
}


void init_keyboard() {
	tcgetattr(0,&initial_settings);
	new_settings = initial_settings;
	new_settings.c_lflag &= ~ICANON;
	new_settings.c_lflag &= ~ECHO;
	new_settings.c_lflag &= ~ISIG;
	new_settings.c_cc[VMIN] = 1;
	new_settings.c_cc[VTIME] = 0;
	tcsetattr(0, TCSANOW, &new_settings);
}

void close_keyboard() {
	tcsetattr(0, TCSANOW, &initial_settings);
}


int kbhit(){
	char ch;
	int nread;
	if( peek_character != -1 )
		return 1;

	new_settings.c_cc[VMIN]=0;
	tcsetattr(0, TCSANOW, &new_settings);
	nread = read(0,&ch,1);
	new_settings.c_cc[VMIN]=1;
	tcsetattr(0, TCSANOW, &new_settings);

	if( nread == 1 ){
		peek_character = ch;
		return 1;
	}

	return 0;
}

int readch() {
	char ch;
	if( peek_character != -1 ){
		ch = peek_character;
		peek_character = -1;
		return ch;
	}
	read(0, &ch, 1);
	return ch;
}
