/*
! /traductorBraille/software/arduino/include

	Traductor Braile es un dispositivo creado para que personas no
	videntes, puedan acceder a textos literarios digitales.
	- 2022 Traductor Braile Team
	(https://gitlab.com/traductor-braille/software)
*/

/* Este programa se almacena en la memoria flash de arduino y su
 * funcion es leer una memoria SD con textos digitales y representar
 * cracter por caracter en una matriz de pines fisicos. Al conectarlo
 * por puerto serie se puede decidir si se lee el modulo SD o se le
 * envia texto mediante conexion serial. */

/* Posiciones Servo 1 */
#ifndef ANGLES_SERVOS
#define ANGLES_SERVOS

#define S1_00 0
#define S1_10 155
#define S1_01 95
#define S1_11 45

/* Posiciones Servo 2 */
#define S2_00 0
#define S2_10 95
#define S2_01 145
#define S2_11 45

/* Posiciones Servo 3 */
#define S3_00 0
#define S3_10 155
#define S3_01 110
#define S3_11 55

#endif
