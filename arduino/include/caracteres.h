/*
! /traductorBraille/software/arduino/include

	Traductor Braile es un dispositivo creado para que personas no
	videntes, puedan acceder a textos literarios digitales.
	- 2022 Traductor Braile Team
	(https://gitlab.com/traductor-braille/software)
*/

/* Este programa se almacena en la memoria flash de arduino y su
 * funcion es leer una memoria SD con textos digitales y representar
 * cracter por caracter en una matriz de pines fisicos. Al conectarlo
 * por puerto serie se puede decidir si se lee el modulo SD o se le
 * envia texto mediante conexion serial. */

#ifndef CARACTERES
#define CARACTERES

#include "angles_servo.h"

typedef struct{
	int s1;
	int s2;
	int s3;
} position_t;

position_t simbol[] = {
	/*number*/ simbol[0].s1 = S1_01, simbol[0].s2 = S2_01, simbol[0].s3 = S3_11
};

position_t state[] = {
	/* */  state[0].s1 = S1_00,   state[0].s2 = S2_00,   state[0].s3 = S3_00,
	/*!*/  state[1].s1 = S1_00,   state[1].s2 = S2_11,   state[1].s3 = S3_10,
	/*"*/  state[2].s1 = S1_00,   state[2].s2 = S2_10,   state[2].s3 = S3_11,
	/*#*/  state[3].s1 = S1_01,   state[3].s2 = S2_01,   state[3].s3 = S3_11,
	/*$*/  state[4].s1 = S1_00,   state[4].s2 = S2_00,   state[4].s3 = S3_00,
	/*%*/  state[4].s1 = S1_00,   state[4].s2 = S2_00,   state[4].s3 = S3_00,
	/*&*/  state[6].s1 = S1_00,   state[6].s2 = S2_00,   state[6].s3 = S3_00,
	/*'*/  state[7].s1 = S1_00,   state[7].s2 = S2_00,   state[7].s3 = S3_10,
	/*(*/  state[8].s1 = S1_00,   state[8].s2 = S2_11,   state[8].s3 = S3_11,
	/*)*/  state[9].s1 = S1_00,   state[9].s2 = S2_11,   state[9].s3 = S3_11,
	/***/ state[10].s1 = S1_00,  state[10].s2 = S2_01,  state[10].s3 = S3_10,
	/*+*/ state[11].s1 = S1_00,  state[11].s2 = S2_11,  state[11].s3 = S3_10,
	/*,*/ state[12].s1 = S1_00,  state[12].s2 = S2_10,  state[12].s3 = S3_00,
	/*-*/ state[13].s1 = S1_00,  state[13].s2 = S2_11,  state[13].s3 = S3_00,
	/*.*/ state[14].s1 = S1_00,  state[14].s2 = S2_00,  state[14].s3 = S3_10,
	/* */ state[15].s1 = S1_01,  state[15].s2 = S2_00,  state[15].s3 = S3_10,
	/*0*/ state[16].s1 = S1_01,  state[16].s2 = S2_11,  state[16].s3 = S3_00,
	/*1*/ state[17].s1 = S1_10,  state[17].s2 = S2_00,  state[17].s3 = S3_00,
	/*2*/ state[18].s1 = S1_10,  state[18].s2 = S2_10,  state[18].s3 = S3_00,
	/*3*/ state[19].s1 = S1_11,  state[19].s2 = S2_00,  state[19].s3 = S3_00,
	/*4*/ state[20].s1 = S1_11,  state[20].s2 = S2_01,  state[20].s3 = S3_00,
	/*5*/ state[21].s1 = S1_10,  state[21].s2 = S2_01,  state[21].s3 = S3_00,
	/*6*/ state[22].s1 = S1_11,  state[22].s2 = S2_10,  state[22].s3 = S3_00,
	/*7*/ state[23].s1 = S1_11,  state[23].s2 = S2_11,  state[23].s3 = S3_00,
	/*8*/ state[24].s1 = S1_10,  state[24].s2 = S2_11,  state[24].s3 = S3_00,
	/*9*/ state[25].s1 = S1_01,  state[25].s2 = S2_10,  state[25].s3 = S3_00,
	/*:*/ state[26].s1 = S1_00,  state[26].s2 = S2_11,  state[26].s3 = S3_00,
	/*;*/ state[27].s1 = S1_00,  state[27].s2 = S2_10,  state[27].s3 = S3_10,
	/*<*/ state[28].s1 = S1_10,  state[28].s2 = S2_10,  state[28].s3 = S3_01,
	/*=*/ state[29].s1 = S1_00,  state[29].s2 = S2_11,  state[29].s3 = S3_11,
	/*>*/ state[30].s1 = S1_01,  state[30].s2 = S2_01,  state[30].s3 = S3_10,
	/*?*/ state[31].s1 = S1_00,  state[31].s2 = S2_10,  state[31].s3 = S3_11,
	/*@*/ state[32].s1 = S1_01,  state[32].s2 = S2_01,  state[32].s3 = S3_10,
	/*A*/ state[33].s1 = S1_10,  state[33].s2 = S2_00,  state[33].s3 = S3_00,
	/*B*/ state[34].s1 = S1_10,  state[34].s2 = S2_10,  state[34].s3 = S3_00,
	/*C*/ state[35].s1 = S1_11,  state[35].s2 = S2_00,  state[35].s3 = S3_00,
	/*D*/ state[36].s1 = S1_11,  state[36].s2 = S2_01,  state[36].s3 = S3_00,
	/*E*/ state[37].s1 = S1_10,  state[37].s2 = S2_01,  state[37].s3 = S3_00,
	/*F*/ state[38].s1 = S1_11,  state[38].s2 = S2_10,  state[38].s3 = S3_00,
	/*G*/ state[39].s1 = S1_11,  state[39].s2 = S2_11,  state[39].s3 = S3_00,
	/*H*/ state[40].s1 = S1_10,  state[40].s2 = S2_11,  state[40].s3 = S3_00,
	/*I*/ state[41].s1 = S1_01,  state[41].s2 = S2_10,  state[41].s3 = S3_00,
	/*J*/ state[42].s1 = S1_01,  state[42].s2 = S2_11,  state[42].s3 = S3_00,
	/*K*/ state[43].s1 = S1_10,  state[43].s2 = S2_00,  state[43].s3 = S3_10,
	/*L*/ state[44].s1 = S1_10,  state[44].s2 = S2_10,  state[44].s3 = S3_10,
	/*M*/ state[45].s1 = S1_11,  state[45].s2 = S2_00,  state[45].s3 = S3_10,
	/*N*/ state[46].s1 = S1_11,  state[46].s2 = S2_01,  state[46].s3 = S3_10,
	/*O*/ state[47].s1 = S1_10,  state[47].s2 = S2_01,  state[47].s3 = S3_10,
	/*P*/ state[48].s1 = S1_11,  state[48].s2 = S2_10,  state[48].s3 = S3_10,
	/*Q*/ state[49].s1 = S1_11,  state[49].s2 = S2_11,  state[49].s3 = S3_10,
	/*R*/ state[50].s1 = S1_10,  state[50].s2 = S2_11,  state[50].s3 = S3_10,
	/*S*/ state[51].s1 = S1_01,  state[51].s2 = S2_10,  state[51].s3 = S3_10,
	/*T*/ state[52].s1 = S1_01,  state[52].s2 = S2_11,  state[52].s3 = S3_10,
	/*U*/ state[53].s1 = S1_10,  state[53].s2 = S2_00,  state[53].s3 = S3_11,
	/*V*/ state[54].s1 = S1_10,  state[54].s2 = S2_10,  state[54].s3 = S3_11,
	/*W*/ state[55].s1 = S1_10,  state[55].s2 = S2_00,  state[55].s3 = S3_00,
	/*X*/ state[56].s1 = S1_11,  state[56].s2 = S2_00,  state[56].s3 = S3_11,
	/*Y*/ state[57].s1 = S1_11,  state[57].s2 = S2_01,  state[57].s3 = S3_11,
	/*Z*/ state[58].s1 = S1_10,  state[58].s2 = S2_01,  state[58].s3 = S3_11,
	/*[*/ state[59].s1 = S1_00,  state[59].s2 = S2_11,  state[59].s3 = S3_11,
	/*\*/ state[60].s1 = S1_00,  state[60].s2 = S2_00,  state[60].s3 = S3_00,
	/*]*/ state[61].s1 = S1_00,  state[61].s2 = S2_11,  state[61].s3 = S3_11,
	/*^*/ state[62].s1 = S1_00,  state[62].s2 = S2_00,  state[62].s3 = S3_00,
	/*_*/ state[63].s1 = S1_00,  state[63].s2 = S2_00,  state[63].s3 = S3_11,
	/* */ state[63].s1 = S1_00,  state[63].s2 = S2_00,  state[63].s3 = S3_00,
	/*a*/ state[65].s1 = S1_10,  state[65].s2 = S2_00,  state[65].s3 = S3_00,
	/*b*/ state[66].s1 = S1_10,  state[66].s2 = S2_10,  state[66].s3 = S3_00,
	/*c*/ state[67].s1 = S1_11,  state[67].s2 = S2_00,  state[67].s3 = S3_00,
	/*d*/ state[68].s1 = S1_11,  state[68].s2 = S2_01,  state[68].s3 = S3_00,
	/*e*/ state[69].s1 = S1_10,  state[69].s2 = S2_01,  state[69].s3 = S3_00,
	/*f*/ state[70].s1 = S1_11,  state[70].s2 = S2_10,  state[70].s3 = S3_00,
	/*g*/ state[71].s1 = S1_11,  state[71].s2 = S2_11,  state[71].s3 = S3_00,
	/*h*/ state[72].s1 = S1_10,  state[72].s2 = S2_11,  state[72].s3 = S3_00,
	/*i*/ state[73].s1 = S1_01,  state[73].s2 = S2_10,  state[73].s3 = S3_00,
	/*j*/ state[74].s1 = S1_01,  state[74].s2 = S2_11,  state[74].s3 = S3_00,
	/*k*/ state[75].s1 = S1_10,  state[75].s2 = S2_00,  state[75].s3 = S3_10,
	/*l*/ state[76].s1 = S1_10,  state[76].s2 = S2_10,  state[76].s3 = S3_10,
	/*m*/ state[77].s1 = S1_11,  state[77].s2 = S2_00,  state[77].s3 = S3_10,
	/*n*/ state[78].s1 = S1_11,  state[78].s2 = S2_01,  state[78].s3 = S3_10,
	/*o*/ state[79].s1 = S1_10,  state[79].s2 = S2_01,  state[79].s3 = S3_10,
	/*p*/ state[80].s1 = S1_11,  state[80].s2 = S2_10,  state[80].s3 = S3_10,
	/*q*/ state[81].s1 = S1_11,  state[81].s2 = S2_11,  state[81].s3 = S3_10,
	/*r*/ state[82].s1 = S1_10,  state[82].s2 = S2_11,  state[82].s3 = S3_10,
	/*s*/ state[83].s1 = S1_01,  state[83].s2 = S2_10,  state[83].s3 = S3_10,
	/*t*/ state[84].s1 = S1_01,  state[84].s2 = S2_11,  state[84].s3 = S3_10,
	/*u*/ state[85].s1 = S1_10,  state[85].s2 = S2_00,  state[85].s3 = S3_11,
	/*v*/ state[86].s1 = S1_10,  state[86].s2 = S2_10,  state[86].s3 = S3_11,
	/*w*/ state[87].s1 = S1_10,  state[87].s2 = S2_00,  state[87].s3 = S3_00,
	/*x*/ state[88].s1 = S1_11,  state[88].s2 = S2_00,  state[88].s3 = S3_11,
	/*y*/ state[89].s1 = S1_11,  state[89].s2 = S2_01,  state[89].s3 = S3_11,
	/*z*/ state[90].s1 = S1_10,  state[90].s2 = S2_01,  state[90].s3 = S3_11,
	/*{*/ state[91].s1 = S1_00,  state[91].s2 = S2_11,  state[91].s3 = S3_11,
	/*|*/ state[92].s1 = S1_00,  state[92].s2 = S2_00,  state[92].s3 = S3_00,
	/*}*/ state[93].s1 = S1_00,  state[93].s2 = S2_11,  state[93].s3 = S3_11
};

#endif
