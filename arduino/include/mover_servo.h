/*
! /traductorBraille/software/arduino/include

	Traductor Braile es un dispositivo creado para que personas no
	videntes, puedan acceder a textos literarios digitales.
	- 2022 Traductor Braile Team
	(https://gitlab.com/traductor-braille/software)
*/

/* Este programa se almacena en la memoria flash de arduino y su
 * funcion es leer una memoria SD con textos digitales y representar
 * cracter por caracter en una matriz de pines fisicos. Al conectarlo
 * por puerto serie se puede decidir si se lee el modulo SD o se le
 * envia texto mediante conexion serial. */
#ifndef MOVE_SERVO
#define MOVE_SERVO

#include <Arduino.h>        // Biblioteca de arduino para poder utilizar
#include <Servo.h>          // Biblioteca para poder implementar Servos
#include <SoftwareSerial.h> //Biblioteca para conexion serial

Servo servo_1, servo_2, servo_3; /* Definicion de servomotores */

/* La funcion 'mover_servo()' recibe el caracter ASCII leido desde el
 * SD o enviado desde puerto serie y lo utiliza para mover los
 * servomotores con la utilizacion del header caracteres.h */
void mover_servo(int caracter){
	Serial.println(F ("Funcion mover servo.") ); /* Linea de debug */
	Serial.print(F ("Entero recibido: ") ); /* Linea de debug */
	Serial.println(caracter); /* Linea de debug */
	Serial.print(F ("Elementos arreglo struct: ") ); /* Linea de debug */
	Serial.println(caracter - ' '); /* Linea de debug */

	if( (caracter > 47) && (caracter < 58) ){
		/* La funcion 'write()' le manda a los objetos servo* la posicion
		 * a la que estos deben moverse para formar el caracter recibido
		 * por la funcion 'mover_servo()' */
		servo_1.write(simbol[0].s1);
		servo_2.write(simbol[0].s2);
		servo_3.write(simbol[0].s3);
		delay(1000);
	}

	servo_1.write(state[caracter - ' '].s1);
	servo_2.write(state[caracter - ' '].s2);
	servo_3.write(state[caracter - ' '].s3);
	/* La funcion delay() pausa el programa por una cantidad establecida
	 * de milisegundos. Se utiliza para regular la muestra de caracteres
	 * en la traduccion a braille */
}
#endif
