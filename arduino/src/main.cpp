/*
! /traductorBraille/software/arduino/src

	Traductor Braile es un dispositivo creado para que personas no
	videntes, puedan acceder a textos literarios digitales.
	- 2022 Traductor Braile Team
	(https://gitlab.com/traductor-braille/software)
*/

/* Este programa se almacena en la memoria flash de arduino y su
 * funcion es leer una memoria SD con textos digitales y representar
 * cracter por caracter en una matriz de pines fisicos. Al conectarlo
 * por puerto serie se puede decidir si se lee el modulo SD o se le
 * envia texto mediante conexion serial. */

#include <Arduino.h>        /* Biblioteca de arduino para poder utilizar
															 estructura de lenguaje arduino */
#include <Servo.h>          // Biblioteca para poder implementar Servos
#include <SD.h>             // Biblioteca control y manejo de modulo SD
#include <SPI.h>            //Biblioteca para conexion con modulo SD
#include <SoftwareSerial.h> //Biblioteca para conexion serial

#include "caracteres.h"     //Header con caracteres ASCII a braille
#include "mover_servo.h"    //Header para funcion mover servo

#define SERVO1 6
#define SERVO2 7
#define SERVO3 8

const int buttonChangeDelay = 2; /* Change next directory */
const int buttonStartPause = 3; /* 0 Pause - 1 Start*/
const int buttonRewindLetter = 4; /* Rewind one caracter back */

int mode; /* Modo lectura de SD o modo lectura de puerto serie */

unsigned long previousMillis = 0; /* Guarda instante tiempo pasado*/
unsigned long interval = 500; /* Intervalo de delay */
unsigned long inicial_delay = 5000; /* Delay para eleccion */

File dataFile; /* Definicion del modulo SD*/
int dataLine; /* Cadena para almacenar texto de archivo*/

/* La funcion 'setup' se ejecuta al encender placa o reset */
void setup(){
	/* 'Serial.begin()' inicializa el puerto serie (UART) a 9600 bps,
	 * con el formato 8N1 (por defecto) full duplex. Esto permite la
	 * comunicacion del arduino con la computadora */
	Serial.begin (9600, SERIAL_8N1);

	/* Espera a establecer conexion con puerto serial */
	while(!Serial)
		; /* Bucle hasta que el puerto serial sea conectado*/

/* Declaracion de servomotores en su pin digital correspondiente */
	servo_1.attach(SERVO1);
	servo_2.attach(SERVO2);
	servo_3.attach(SERVO3);

	/* 'SD.begin()' inicializa la biblioteca SD y conecta con la tarjeta.
	 * Devuelve 1 si se conecto con exito o 0 si la conexion fallo */
	if( SD.begin(10) == 0 ){
		/* La funcion 'println()' nos permite imprimir un texto, y realiza
		 * un salto de linea */ 
		Serial.println(F ("\nError loading SD") );
		return;
	}
		Serial.println(F ("\nSD successfully loaded.") ); 

	/* Abre archivo 'datos.txt' que se encuentra en la tarjeta SD en modo
	 * para solo lectura */
	dataFile = SD.open("datos.txt", FILE_READ);
	Serial.println(F ("\nArchivo 'datos.txt', abierto en modo lectura.") );

	/* Configuracion de entrada de pulsadores */
	pinMode(buttonStartPause, INPUT);
	pinMode(buttonRewindLetter, INPUT);
	pinMode(buttonChangeDelay, INPUT);

}

/* La funcion 'loop()' se ejecuta indetermindamente en bucle */
void loop(){
	unsigned long currentMillis = millis();

	Serial.println("\nWelcome to Traductor Braille Machine");
	Serial.println("Choose the mode you want:");
	Serial.println("\tPress 0 to Read SD Mode.");
	Serial.println("\tPress 1 to Serial Comunication Mode.\n");

	do{
		while( Serial.available() == 0 )
			; /* Es 0 cuando hay datos en el buffer disponibles para leer */

			/* Serial read lee los bytes del numero enviado por la PC que llega al
			 * buffer */
			mode = Serial.read();
			/* Chequeo que los bytes pasen correctamente */
			Serial.println("Caracter ASCII impreso: "); /* Linea de debug */
			Serial.println(mode); /* Linea de debug */

		currentMillis = millis();
		if (currentMillis - previousMillis >= inicial_delay)
			mode = 0;

		} while(mode != '0' && mode != '1');

	if(mode == '0')
		Serial.println("Read SD Mode.");
	else
		Serial.println("Serial comunication Mode.");

	Serial.println("Restart to return to the menu.\n");

	/* Los posibles modos de funcionamiento son:
	 * 0 modo lectura de SD (default)
	 * 1 modo comunicacion Serial*/
	switch(mode){
		case '0': // Modo lectura de SD
			/* La funcion 'available()' comprueba si hay bytes disponibles para
			 * leer en el archivo 'dataFile'. Devuelve 1 si hay bytes para leer y
			 * 0 en caso contrario */
			while( dataFile.available() ){
				/* Delay sin retardo. Nos permite mantener la funcion de lectura de
				 * pulsadores mientras se muestran los caracteres el la matriz
				 * braille */
				currentMillis = millis();

				if (currentMillis - previousMillis >= interval){
					previousMillis = currentMillis; // Delay no bloqueante

					/* Accion de botones pulsaldos */
					/* La funcion 'digitalRead()' lee el estado del pulsador y devuelve
					 * 1 si el pulsador esta presionado o 0 de lo contrario */
					if( digitalRead(buttonStartPause) ){ // Lectura de pulsador
						delay(300); // Retardo hasta soltar pulsador
						Serial.println(F ("\n\nButton pause pressed.") ); /* Debug */
						do{} while( !digitalRead(buttonStartPause) );
						Serial.println(F ("Button start pressed.\n\n") ); /* Debug */
					} // Pausa hasta volver a pulsar

					if( digitalRead(buttonRewindLetter) ){ // Lectura de pulsador
						delay(300); // Retardo hasta soltar pulsador
						/* La funcion 'seek()' desplaza un apuntador que indica el proximo
						 * byte a leer/escribir dentro del archivo */
						/* La funcion 'position()' devuelve la posicion donde se leera o
						 * escribira dentro del archivo */
						dataFile.seek( dataFile.position() - 1 );
						Serial.println(F ("\n\nButton rewind letter pressed.\n\n") ); /* Debug */
					}

					if( digitalRead(buttonChangeDelay) ){
						delay(300); // Retardo hasta soltar pulsador
						interval += 1000;
						if (interval == 5500)
							interval = 500;
						Serial.println(F ("\n\nButton change delay pressed.") ); /* Debug */
						Serial.print("Delay in:"); /* Debug */
						Serial.println(interval); /* Debug */
						Serial.println("\n\n"); /* Debug */
					}

					Serial.println(F ("Dentro del delay no bloqueante") ); /* Debug */

					/* La funcion 'read()' lee un byte de datos (un caracter) del archivo
					 * de texto de la SD y lo guarda en un entero. Entonces lee un
					 * caracter y guarda en 'dataLine' su correspondiente en ASCII */
					dataLine = dataFile.read();
					/* Se llama a la funcion 'mover_servo()' y se le pasa 'dataLine'*/
					mover_servo(dataLine);
				}
			}
			/* Se cierra el archivo que contiene 'dataFile' */
			dataFile.close();

			Serial.println(F ("Archivo de texto copiado.") ); /* Linea de debug */
			break;

		case '1': // Modo de comunicacion serial
			for(;;){
				/* Delay sin retardo. Nos permite mantener la funcion de lectura de
				 * pulsadores mientras se muestran los caracteres el la matriz
				 * braille */
				currentMillis = millis();

				if (currentMillis - previousMillis >= interval){
					previousMillis = currentMillis; // Delay no bloqueante
					/* Si el buffer esta vacio [0], espera */
					/* Si se manda algo al buffer [1], sale del while */
					while( Serial.available() == 0 )
						; /* Lee los bytes del numero enviado por la PC que llega al
							 * buffer */

					/* Accion de botones pulsaldos */
					/* La funcion 'digitalRead()' lee el estado del pulsador y devuelve
					 * 1 si el pulsador esta presionado o 0 de lo contrario */
					if( digitalRead(buttonStartPause) ){ // Lectura de pulsador
						delay(300); // Retardo hasta soltar pulsador
						Serial.println(F ("\n\nButton pause pressed.") ); /* Debug */
						do{} while( !digitalRead(buttonStartPause) );
						Serial.println(F ("Button start pressed.\n\n") ); /* Debug */
					} // Pausa hasta volver a pulsar

					if( digitalRead(buttonRewindLetter) ){ // Lectura de pulsador
						delay(300); // Retardo hasta soltar pulsador
						/* La funcion 'seek()' desplaza un apuntador que indica el proximo
						 * byte a leer/escribir dentro del archivo */
						/* La funcion 'position()' devuelve la posicion donde se leera o
						 * escribira dentro del archivo */
						dataFile.seek( dataFile.position() - 1 );
						Serial.println(F ("\n\nButton rewind letter pressed.\n\n") ); /* Debug */
					}

					if( digitalRead(buttonChangeDelay) ){
						delay(300); // Retardo hasta soltar pulsador
						interval += 1000;
						if (interval == 5500)
							interval = 500;
						Serial.println(F ("\n\nButton change delay pressed.") ); /* Debug */
						Serial.print("Delay in:"); /* Debug */
						Serial.println(interval); /* Debug */
						Serial.println("\n\n"); /* Debug */
					}

					/* Serial read lee los bytes del numero enviado por la PC que
					 * llega al buffer */
					dataLine = Serial.read();
					/* Chequeo que los bytes pasen correctamente */
					Serial.println(dataLine); /* Linea de debug */

					Serial.print("Dato enviado: "); /* Linea de debug */
					Serial.println(dataLine);
					/* Estado de cada servomotor */
					mover_servo(dataLine);
				}
			}
			break;
	}
}
